﻿using LibraryDAL.Context;
using LibraryDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryDAL.Repository
{
    public class AuthorRepository : IRepository<Author>
    {
        private LibraryContext _libraryContext;

        public AuthorRepository(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }

        public bool Create(Author entity)
        {
            var authors = _libraryContext.Authors.ToList();

            authors.Add(entity);

            if (authors.Contains(entity))
            {
                return true;
            }

            return false;
        }

        public bool Delete(Guid id)
        {
            var authors = _libraryContext.Authors.ToList();

            var authorToDelete = authors.Find(a => a.Id == id);

            if (authorToDelete == null)
            {
                return false;
            }

            authors.Remove(authorToDelete);
            return true;
        }

        public IEnumerable<Author> GetAll()
        {
            var authors = _libraryContext.Authors;

            return authors;
        }

        public Author GetEntityById(Guid id)
        {
            var authors = _libraryContext.Authors.ToList();

            var author = authors.FirstOrDefault(a => a.Id == id);

            if(author == null)
            {
                throw new InvalidOperationException("There is no author with such id");
            }

            return author;
        }

        public Author Update(Author newEntity)
        {
            var author = _libraryContext.Authors.FirstOrDefault(a => a.Id == newEntity.Id);

            if(author == null)
            {
                throw new InvalidOperationException("There is no author with such id");
            }

            author.FirstName = newEntity.FirstName;
            author.LastName = newEntity.LastName;
            author.MiddleName = newEntity.MiddleName;
            author.DateOfBirth = newEntity.DateOfBirth;
            //author.Books = newEntity.Books;

            return author;
        }
    }
}