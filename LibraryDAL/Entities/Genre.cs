﻿namespace LibraryDAL.Entities
{
    public enum Genre
    {
        Fiction = 0,
        Novel = 1,
        Adventures = 2,
        Documentary = 3,
        Pomance = 4,
        Periodic = 5,
    }
}