﻿using LibraryDAL.Context;
using LibraryDAL.Entities;
using LibraryDAL.Repository;
using LibraryDAL.UoW;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryDAL.Infrastructure
{
    public class ServiceConfigurator
    {
        public static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<LibraryContext>();
            serviceCollection.AddTransient<IRepository<Author>, AuthorRepository>();
            // serviceCollection.AddTransient<IRepository<Book>, BookRepository>(); - implement, please BookRepository and uncomment this
            serviceCollection.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}
