﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryBLL.Dto
{
    public class AuthorDto
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; } // FirstName + LastName - we don`t have such prop in Author entity

        public string DateOfBirth { get; set; }

        public string DateOfDeath { get;set; }

        public IEnumerable<Guid> Books { get; set; } // we have different type of this collection in Author entity
    }
}
