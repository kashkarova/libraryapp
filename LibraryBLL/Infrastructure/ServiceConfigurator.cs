﻿using AutoMapper;
using LibraryBLL.Services;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryBLL.Infrastructure
{
    public class ServiceConfigurator 
    {
        public static void ConfigureServices(IServiceCollection serviceCollection)
        {
            LibraryDAL.Infrastructure.ServiceConfigurator.ConfigureServices(serviceCollection);

            serviceCollection.AddTransient<IAuthorService, AuthorService>();
            // serviceCollection.AddTransient<IBookService, BookService>(); - we have to implement book service

            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MapperConfigurator(cfg));
            });

            serviceCollection.AddSingleton<IMapper>(mapperConfiguration.CreateMapper()); // registering mapper, we don`t need register it any more on Startup.cs
        }
    }
}
