﻿using AutoMapper;
using LibraryBLL.Dto;
using LibraryDAL.Entities;
using System.Linq;

namespace LibraryBLL.Infrastructure
{
    public class MapperConfigurator : Profile
    {
        public MapperConfigurator(IMapperConfigurationExpression configExpression)
        {
            // no MapperConfiguration created here - it has already created in ServiceConfigurator.cs
            configExpression.CreateMap<Author, AuthorDto>()
            .ForMember(destination => destination.FullName,
                option => option.MapFrom(source => $"{source.FirstName} {source.LastName}"))
             .ForMember(destination => destination.DateOfBirth,
                option => option.MapFrom(s => s.DateOfBirth.Date.ToShortDateString()))
            .ForMember(destination => destination.DateOfDeath,
                option => option.MapFrom(s => s.DateOfDeath == null ? "Classics never die!" : s.DateOfBirth.Date.ToShortDateString()))
            .ForMember(destination => destination.Books,
                option => option.MapFrom(s => s.Books.Select(b => b.BookId)))
            .ReverseMap();

            //don`t forget to add mappings for Book and BookDto
        }
    }
}
